package com.ucd.java.jaxb.spike;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class NotificationBo extends Notification {

	private static final long serialVersionUID = 1L;

	private String id;
	private TemplateEnum template;
	private String recipientsEmails;
	private String templateVars;
	private Date lastUpdated;
	private String cc;
	private String replyTo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public TemplateEnum getTemplate() {
		return template;
	}

	public void setTemplate(TemplateEnum template) {
		this.template = template;
	}

	public String getRecipientsEmails() {
		return recipientsEmails;
	}

	public void setRecipientsEmails(String recipientsEmails) {
		this.recipientsEmails = recipientsEmails;
	}

	public String getTemplateVars() {
		return templateVars;
	}

	public void setTemplateVars(String templateVars) {
		this.templateVars = templateVars;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getReplyTo() {
		return replyTo;
	}

	public void setReplyTo(String replyTo) {
		this.replyTo = replyTo;
	}

	@Override
	public String toString() {
		return "NotificationBo [id=" + id + ", template=" + template
				+ ", recipientsEmails=" + recipientsEmails + ", templateVars="
				+ templateVars + ", lastUpdated=" + lastUpdated + ", cc=" + cc
				+ ", replyTo=" + replyTo + "]";
	}

}
