package com.ucd.java.jaxb.spike;

public enum NotificationStatusEnum {

	SENT("Sent"), PENDING("Pending"), FATAL("Fatal");

	private String notificationStatus;

	public String getNotificationStatus() {
		return notificationStatus;
	}

	private NotificationStatusEnum(String notificationStatus) {
		this.notificationStatus = notificationStatus;
	}

}
