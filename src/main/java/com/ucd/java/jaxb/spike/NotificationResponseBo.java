package com.ucd.java.jaxb.spike;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class NotificationResponseBo extends Notification {

	private static final long serialVersionUID = 1L;

	private NotificationBo bo;
	
	private NotificationStatusEnum status;

	public NotificationStatusEnum getStatus() {
		return status;
	}

	public void setStatus(NotificationStatusEnum status) {
		this.status = status;
	}

	public NotificationBo getBo() {
		return bo;
	}

	public void setBo(NotificationBo bo) {
		this.bo = bo;
	}

	@Override
	public String toString() {
		return "NotificationResponseBo [id=" + this.getBo().getId() + ", template="
				+ this.getBo().getTemplate() + ", recipientsEmails="
				+ this.getBo().getRecipientsEmails() + ", templateVars="
				+ this.getBo().getTemplateVars() + ", lastUpdated="
				+ this.getBo().getLastUpdated() + ", status=" + status + ", cc="
				+ this.getBo().getCc() + ", replyTo=" + this.getBo().getReplyTo() + "]";
	}
}
