package com.ucd.java.jaxb.spike;

public enum TemplateEnum {

	BUNDLE_FORWARD_INVITE_SEND(
			"template.bundle_forward_invite_send.body.path",
			"template.bundle_forward_invite_send.subject.path"
			),
	BUNDLE_FORWARD_INVITE_RESOLVED(
			"template.bundle_forward_invite_resolved.body.path",
			"template.bundle_forward_invite_resolved.subject.path"
			),
	BUNDLE_REVERSE_INVITE_SEND(
			"template.bundle_reverse_invite_send.body.path",
			"template.bundle_reverse_invite_send.subject.path"
			),
	BUNDLE_REVERSE_INVITE_RESOLVED(
			"template.bundle_reverse_invite_resolved.body.path",
			"template.bundle_reverse_invite_resolved.subject.path"
			),
	BATCH_JOB_EXIT_STATUS(
			"template.batch_job_exit_status.body.path",
			"template.batch_job_exit_status.subject.path"
			),
	FREE_TEXT_NOTIFICATION(
			"template.free_text_notification.body.path",
			"template.free_text_notification.subject.path"
			);

	private String bodyPathProperty;
	private String subjectPathProperty;

	private TemplateEnum(String b, String s) {
		this.bodyPathProperty = b;
		this.subjectPathProperty = s;
	}

	public String getBodyPathProperty() {
		return bodyPathProperty;
	}

	public String getSubjectPathProperty() {
		return subjectPathProperty;
	}


}
