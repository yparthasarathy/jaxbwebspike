package com.ucd.java.jaxb.spike;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@WebService(name = "SushiNotificationService")
@SOAPBinding(style = SOAPBinding.Style.RPC, use = SOAPBinding.Use.LITERAL)
public class SushiNotificationService extends SpringBeanAutowiringSupport {

	@WebMethod(operationName = "createNotification", action = "createNotification")
	@WebResult(name = "notificationId")
	public String createNotification(
			@WebParam(name = "notification") Notification notification) {
		NotificationBo bo = (NotificationBo) notification;
		System.out.println("bo: %o" + bo.toString());
		return "Id created";
	}

	@WebMethod(operationName = "deleteNotification", action = "deleteNotification")
	@WebResult(name = "deletedNotificationId")
	public String deleteNotification(
			@WebParam(name = "notificationId") String notificationId) {
		return null;
	}

	@WebMethod(operationName = "getNotification", action = "getNotification")
	@WebResult(name = "notification")
	public Notification getNotification(
			@WebParam(name = "notificationId") String notificationId) {
		NotificationBo request = new NotificationBo();
		request.setCc("cc@test.com");
		request.setId("some random id");
		request.setRecipientsEmails("recipient@test.com");
		request.setReplyTo("replyto@test.com");
		request.setTemplate(TemplateEnum.BATCH_JOB_EXIT_STATUS);
		request.setTemplateVars("{vars : sample json vars}");

		NotificationResponseBo response = new NotificationResponseBo();
		response.setBo(request);
		response.setStatus(NotificationStatusEnum.SENT);
		System.out.println("response: %o" + response.toString());
		return response;
	}

	/*
	 * @WebMethod(operationName = "listNotifications", action =
	 * "listNotifications")
	 * 
	 * @WebResult(name = "notification") public List<Notification>
	 * listNotifications(
	 * 
	 * @WebParam(name = "filter") String filter) { return null; }
	 */

}
