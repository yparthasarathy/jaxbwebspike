package com.ucd.java.jaxb.spike;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {

		NotificationBo request = new NotificationBo();
		request.setCc("cc@test.com");
		request.setId("some random id");
		request.setRecipientsEmails("recipient@test.com");
		request.setReplyTo("replyto@test.com");
		request.setTemplate(TemplateEnum.BATCH_JOB_EXIT_STATUS);
		request.setTemplateVars("{vars : sample json vars}");
		request.setLastUpdated(new Date());

		/*try {

			// File file = new File("C:\\file.xml");
			JAXBContext jaxbContext = JAXBContext
					.newInstance(NotificationBo.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			// jaxbMarshaller.marshal(request, file);
			jaxbMarshaller.marshal(request, System.out);

		} catch (JAXBException e) {
			e.printStackTrace();
		}*/
		
		NotificationResponseBo response = new NotificationResponseBo();
		response.setStatus(NotificationStatusEnum.SENT);
		response.setBo(request);
		
		Notifications list = new Notifications();
		List<Notification> rnr = new ArrayList<Notification>();
		rnr.add(request);
		rnr.add(response);
		list.setReqNresp(rnr);
		
		try {

			File file = new File("C:\\Yoganand\\Projects\\JavaPOC\\src\\main\\java\\com\\ucd\\poc\\JavaPOC\\file.xml");
			JAXBContext jaxbContext = JAXBContext
					.newInstance(Notifications.class);
			
			System.out.println("***************************************");
			System.out.println("");
			System.out.println("-------- MARSHALLING -----------");
			System.out.println("");
			System.out.println("***************************************");
			
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(list, file);
			jaxbMarshaller.marshal(list, System.out);
			
			System.out.println("***************************************");
			System.out.println("");
			System.out.println("-------- UNMARSHALLING -----------");
			System.out.println("");
			System.out.println("***************************************");
			
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Notifications notifications = (Notifications) jaxbUnmarshaller.unmarshal(file);
			
			System.out.println(notifications.getReqNresp().get(0).toString());
			System.out.println(notifications.getReqNresp().get(1).toString());

		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}
}

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class Notifications {
	
	List<Notification> reqNresp;

	public List<Notification> getReqNresp() {
		return reqNresp;
	}

	public void setReqNresp(List<Notification> reqNresp) {
		this.reqNresp = reqNresp;
	}
	
	

}
