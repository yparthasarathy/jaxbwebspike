package com.ucd.java.jaxb.spike;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({ NotificationResponseBo.class, NotificationBo.class })
public class Notification {

	public Notification() {
		super();
		// TODO Auto-generated constructor stub
	}
}
